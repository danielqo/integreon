# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
- 2.6.3

* System dependencies
- Java 8, in order to run Apache Solr in development (required for searching feature).
- MySQL database.

* Configuration

* Database creation
- `rails db:create db:migrate` is enough to get database set up. There is not database initialization.


* How to run the test suite
- `rails spec` will run all tests in the `spec/` folder. 

* Services (job queues, cache servers, search engines, etc.)
This software relies on [Sunspot](http://sunspot.github.io/) and [Sunspot Rails](https://github.com/sunspot/sunspot/tree/master/sunspot_rails) to provide full text search capabilities on top of Apache Solr. For development purposes, an optional packaged Solr instance is installed via [sunsport_solr](https://github.com/sunspot/sunspot/tree/master/sunspot_solr) gem and can be executed via `rails sunspot:solr:run` (foreground) or `rails sunspot:solr:start` (background).

* Deployment instructions
- `bundle install`
- `rails db:create db:migrate`
- `rails sunspot:solr:run`

* Importing CSV file
- `rails integreon:file_import[PATH_TO_CSV_FILE]`


