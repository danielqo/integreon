class FileImportSessionsController < ApplicationController
  def index
    @file_import_sessions = FileImportSession.order(created_at: :desc).paginate(page: params[:page])
  end

  def show
    @file_import_session = FileImportSession.find(params[:id])
  end
end
