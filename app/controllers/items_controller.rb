class ItemsController < ApplicationController
  def index
    category = Category.find_by_id(params[:category_id])
    @items = if category
                Item.with_category(category).paginate(page: params[:page])
             else
                Item.paginate(page: params[:page])
             end
  end

  def show
    @item = Item.find params[:id]
  end
end
