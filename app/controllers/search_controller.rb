# encoding: utf-8
class SearchController < ApplicationController
  
  def index
    unless params[:q].blank?
      @search_result = Sunspot.search Item do
        fulltext params[:q]
        paginate(:page => params[:page], :per_page => 30)
      end
    end
  end

end