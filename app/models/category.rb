# encoding: utf-8

class Category < ApplicationRecord

    validates :name, presence: true

    has_many :items

end
