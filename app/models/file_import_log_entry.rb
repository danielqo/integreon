# encoding: utf-8
class FileImportLogEntry < ApplicationRecord

    validates :line_number, presence: true, numericality: {only_integer: true, greater_than: 0}
    validates :warning, presence: true
    validates :line_content, presence: true 

    belongs_to :file_import_session

end
