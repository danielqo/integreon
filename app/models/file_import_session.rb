# encoding: utf-8
class FileImportSession < ApplicationRecord

    validates :file_name, presence: true

    has_many :file_import_log_entries

end
