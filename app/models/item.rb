# encoding: utf-8

class Item < ApplicationRecord

    validates :title, presence: true
    validates :summary, presence: true
    validates :bates_number, presence: true, format: { with: /[a-z]{3}\d*\-[a-zA-z]{3}\d*/i } # e.g.: ABC000-XYZ123456
    validates :page_count, presence: true,
                           numericality: {
                               only_integer: true,
                               greater_than: 0
                           }
    
    belongs_to :category

    searchable auto_index: true do
        text :title
        text :summary
    end

    class << self
        def with_category(category)
            where(category_id: category.id)
        end
    end
end
