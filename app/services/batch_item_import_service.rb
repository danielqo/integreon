class BatchItemImportService

    class << self

        def import_from_file(file, column_separator='|')
            import_session = FileImportSession.create(file_name: file)
            line_number = 2 #skips header 
            # using CSV.foreach is better for memory consumption rather than 
            # using CSV.read, which loads the entire file into the memory
            CSV.foreach(file, {headers: true, col_sep: column_separator}) do |row| 
                category = Category.find_or_create_by name: row['Category'].upcase
                item = Item.new title: row['Title'], 
                                summary: row['Summary'],
                                bates_number: row['Bates Number'],
                                page_count: row['Page Count'],
                                category: category
                if item.valid?
                    item.save
                else
                    FileImportLogEntry.create line_number: line_number,
                                              warning: item.errors.messages,
                                              line_content: row.to_s,
                                              file_import_session: import_session
                end
                
                line_number += 1
            end
            return import_session
        end

    end
    
end