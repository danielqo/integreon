Rails.application.routes.draw do
  root 'items#index'

  resources :items, only: [:index, :show]
  resources :file_import_sessions, only: [:index, :show]
  match 'search'              => 'search#index',        :controller => 'search',  :via => :get
end
