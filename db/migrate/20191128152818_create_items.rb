class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :title, null: false
      t.text :summary, null: false
      t.string :bates_number, null: false
      t.integer :page_count, null: false
      t.belongs_to :category, index: true
      t.timestamps
    end

    add_index :items, :bates_number
  end
end
