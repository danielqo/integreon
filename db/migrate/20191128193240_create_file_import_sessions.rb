class CreateFileImportSessions < ActiveRecord::Migration[6.0]
  def change
    create_table :file_import_sessions do |t|
      t.string :file_name, null: false
      t.timestamps
    end
    add_index :file_import_sessions, :file_name
  end
end
