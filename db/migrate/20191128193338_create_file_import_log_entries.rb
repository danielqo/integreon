class CreateFileImportLogEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :file_import_log_entries do |t|
      t.integer :line_number, null: false
      t.string :warning, null: false
      t.text :line_content, null: false
      t.belongs_to :file_import_session, index: true
      t.timestamps
    end
  end
end
