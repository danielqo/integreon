require 'csv'

namespace :integreon do

  if defined?(Rails) && (Rails.env == 'development')
    Rails.logger = Logger.new(STDOUT)
  end

  desc "imports items from CSV file"
  task :file_import, [:file] => [:environment] do |task, args|
    file = args[:file]
    if file.blank?
      Rails.logger.error "Error: CSV file cannot be blank."
    else
      Rails.logger.info "Importing CSV file: #{file}"
      import_session = BatchItemImportService.import_from_file file
      Rails.logger.info "Import finished. See more info at https://foo.bar/import_session/#{import_session.id}"
    end
  end

end
