FactoryBot.define do
  factory :file_import_log_entry do
    line_number { 1 }
    warning { "MyString" }
    line_content { "MyText" }
    association :file_import_session, factory: :file_import_session
  end
end
