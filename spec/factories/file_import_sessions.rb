# encoding: utf-8
FactoryBot.define do
  factory :file_import_session do
    file_name { "MyString" }
  end
end
