FactoryBot.define do
  factory :item do
    title { "This is a title" }
    summary { "This may be a very very very long text" }
    bates_number { "ABC1234-DEF567890" }
    page_count { 3 }
    association :category, factory: :category
  end

  factory :invalid_item_no_title do
    title { nil }
    summary { "This may be a very very very long text" }
    bates_number { "ABC1234-DEF567890" }
    page_count { 3 }
  end

  factory :invalid_item_no_summary do
    title { "This is a title" }
    summary { nil }
    bates_number { "ABC1234-DEF567890" }
    page_count { 3 }
  end

  factory :invalid_item_bates_number_incorrect do
    title { "This is a title" }
    summary { "This may be a very very very long text" }
    bates_number { "C1234-DEF" }
    page_count { 3 }
  end

  factory :invalid_item_page_count_zero do
    title { "This is a title" }
    summary { "This may be a very very very long text" }
    bates_number { "ABC1234-DEF567890" }
    page_count { nil }
  end

  factory :invalid_item_no_page_count do
    title { "This is a title" }
    summary { "This may be a very very very long text" }
    bates_number { "ABC1234-DEF567890" }
    page_count { 0 }
  end
  
end
