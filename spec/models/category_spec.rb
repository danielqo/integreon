require 'rails_helper'

RSpec.describe Category, type: :model do
  
  describe "validations" do

    subject { FactoryBot.create :category}

    it { is_expected.to validate_presence_of :name }
    it { is_expected.to have_many :items }


  end

end
