require 'rails_helper'

RSpec.describe FileImportLogEntry, type: :model do
  
  describe 'validations' do
    subject {FactoryBot.create :file_import_log_entry}

    [:line_number, :warning, :line_content].each do |attr|
      it { is_expected.to validate_presence_of attr }
    end
    it {is_expected.to belong_to(:file_import_session) }
  end

end
