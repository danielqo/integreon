require 'rails_helper'

RSpec.describe FileImportSession, type: :model do
  
  describe 'validations' do
    subject {FactoryBot.create :file_import_session}

    it { is_expected.to validate_presence_of :file_name }
    it {is_expected.to have_many(:file_import_log_entries) }
  end

end
