# encoding: utf-8

require 'rails_helper'

RSpec.describe Item, type: :model do
  
  describe 'validations' do
    subject { FactoryBot.create(:item)}
    [:title, :summary, :bates_number, :page_count].each do |attr|
      it { is_expected.to validate_presence_of attr }
    end

    it { is_expected.to validate_numericality_of(:page_count).is_greater_than(0) }
    it { is_expected.to allow_value('ABC123456-DEF0982').for :bates_number  }
    it {is_expected.to belong_to(:category) }
  end

  describe '.with_category' do
    
    before(:each) do 
      @c1 = FactoryBot.create(:category)
      @c2 = FactoryBot.create(:category)
      FactoryBot.create_list(:item, 3, category: @c1)
      FactoryBot.create_list(:item, 5, category: @c2)
    end

    it 'should return only items from a given category' do
      expect(Item.with_category(@c1).count).to eq(3)
      expect(Item.with_category(@c2).count).to eq(5)
      expect(Item.count).to eq(8)
    end

  end

end
