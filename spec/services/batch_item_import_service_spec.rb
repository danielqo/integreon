# encoding: utf-8

require 'rails_helper'

RSpec.describe BatchItemImportService do
  
  describe 'batch import from file' do
    
    before :each do
      @file_name = 'sample_file.csv'
      @file = CSV.generate(col_sep: '|') do |csv|
          csv << ["Category","Title","Summary","Bates Number","Page Count"]
          csv << ['First Category', 'Some Title', 'Long long summary', 'ABC0000-DEF9999', '3']
          csv << ['Second Category', 'Other Title', 'Long long summary', 'ABC1111-JJJ9999', '9']
          csv << ['Second Category', 'Other Title', 'Long long summary but not valid', '', '3']
          csv << ['First Category', 'Cool Title', 'Long long summary not valid either...', 'ABC0000-DEF9999', '']
          csv << ['First Category', 'Some Title', 'Long long summary... yup, right, not valid', 'ABC0000-DEF9999', '0']
      end

      it "should import only two items from the file" do
        expect(File).to receive(:open).with(@sample_file, "r").and_return(@file)  
        BatchItemImportService.import_from_file @file_name
        expect(Item.count).to eq(2)
        expect(Category.count).to eq(2)
      end
    end


  end

end
